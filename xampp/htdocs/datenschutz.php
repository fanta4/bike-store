<html>
    
<head>
 <link rel="stylesheet" href="includes/styles/Stylesheet.css"> 
</head>
    
<body>
	<?php
		require("menu.php");
		?>
    
	<div id="Datenschutz">
		<div align=“center“>
			<h2>Datenschutzerkl&auml;rung</h2>
		</div>
		<br />
		<div align=“justify“>
			Wir erheben, verwenden und speichern Ihre personenbezogenen Daten ausschlie&szlig;lich im 
			Rahmen der Bestimmungen des Bundesdatenschutzgesetzes der Bundesrepublik Deutschland. 
			Nachfolgend unterrichten wir Sie &uuml;ber Art, Umfang und Zweck der Datenerhebung und Verwendung.
			<br /><br />
			<b>Erhebung und Verarbeitung von Daten</b><p>
			<div align=“justify“>
				Jeder Zugriff auf unsere Internetseite und jeder Abruf einer auf dieser Website hinterlegten 
				Datei werden protokolliert. Die Speicherung dient internen systembezogenen und statistischen 
				Zwecken. Protokolliert werden: Name der abgerufenen Datei, Datum und Uhrzeit des Abrufs, 
				&uuml;bertragene Datenmenge, Meldung &uuml;ber erfolgreichen Abruf, Webbrowser und anfragende 
				Domain. Zus&auml;tzlich werden die IP Adressen der anfragenden Rechner protokolliert. Weitergehende 
				personenbezogene Daten werden nur erfasst, wenn der Nutzer der Website und/oder Kunde Angaben 
				freiwillig, etwa im Rahmen einer Anfrage oder Registrierung oder zum Abschluss eines Vertrages 
				oder &uuml;ber die Einstellungen seines Browsers t&auml;tigt.
				Unsere Internetseite verwendet Cookies. Ein Cookie ist eine Textdatei, die beim Besuch einer Internetseite 
				verschickt und auf der Festplatte des Nutzer der Website und/oder Kunden zwischengespeichert wird. Wird 
				der entsprechende Server unserer Webseite erneut vom Nutzer der Website und/oder Kunden aufgerufen, 
				sendet der Browser des Nutzers der Wesbite und/oder des Kunden den zuvor empfangenen Cookie wieder 
				zur&uuml;ck an den Server. Der Server kann dann die durch diese Prozedur erhaltenen Informationen auf 
				verschiedene Arten auswerten. Durch Cookies k&ouml;nnen z. B. Werbeeinblendungen gesteuert oder das Navigieren 
				auf einer Internetseite erleichtert werden. Wenn der Nutzer der Website und/oder Kunde die Nutzung von Cookies 
				unterbinden will, kann er dies durch lokale Vornahme der &Auml;nderungen seiner Einstellungen in dem auf seinem 
				Computer verwendeten Internetbrowser, also dem Programm zum &Ouml;ffnen und Anzeigen von Internetseiten (z.B. 
				Internet Explorer, Mozilla Firefox, Opera oder Safari) tun.
			</div><br />
			<b>Nutzung und Weitergabe personenbezogener Daten</b><p>
			<div align=“justify“>
				Soweit der Nutzer unserer Webseite personenbezogene Daten zur Verf&uuml;gung gestellt hat, verwenden wir diese 
				nur zur Beantwortung von Anfragen des Nutzers der Website und/oder Kunden, zur Abwicklung mit dem Nutzer der Website und/oder 
				Kunden geschlossener Vertr&auml;ge und f&uuml;r die technische Administration. Personenbezogene Daten werden von uns an Dritte 
				nur weitergegeben oder sonst &uuml;bermittelt, wenn dies zum Zwecke der Vertragsabwicklung oder zu Abrechnungszwecken 
				erforderlich ist oder der Nutzer der Website und/oder Kunde zuvor eingewilligt hat. Der Nutzer der Website und/oder 
				Kunde hat das Recht, eine erteilte Einwilligung mit Wirkung f&uuml;r die Zukunft jederzeit zu widerrufen.

				Die L&ouml;schung der gespeicherten personenbezogenen Daten erfolgt, wenn der Nutzer der Website und/oder Kunde die 
				Einwilligung zur Speicherung widerruft, wenn ihre Kenntnis zur Erf&uuml;llung des mit der Speicherung verfolgten Zwecks nicht 
				
				mehr erforderlich ist oder wenn ihre Speicherung aus sonstigen gesetzlichen Gr&uuml;nden unzul&auml;ssig ist. Daten f&uuml;r 
				Abrechnungszwecke und buchhalterische Zwecke werden von einem L&ouml;schungsverlangen nicht ber&uuml;hrt. zwischen Ihnen und uns 
				ein Vertragsverh&auml;ltnis begr&uuml;ndet, inhaltlich ausgestaltet oder ge&auml;ndert werden soll, erheben und verwenden wir 
				personenbezogene Daten von Ihnen, soweit dies zu diesen Zwecken erforderlich ist.
				<br />Auf Anordnung der zust&auml;ndigen Stellen d&uuml;rfen wir im Einzelfall Auskunft &uuml;ber diese Daten (Bestandsdaten) 
				erteilen, soweit dies f&uuml;r Zwecke der Strafverfolgung, zur Gefahrenabwehr, zur Erf&uuml;llung der gesetzlichen Aufgaben 
				der Verfassungsschutzbeh&ouml;rden oder des Milit&auml;rischen Abschirmdienstes oder zur Durchsetzung der Rechte am geistigen 
				Eigentum erforderlich ist.
			</div><br />
			<b>Auskunftsrecht</b><p>
			<div align=“justify“>
				Auf schriftliche Anfrage informieren wir den Nutzer der Website und/oder den Kunden &uuml;ber die zu seiner Person 
				gespeicherten Daten. Die Anfrage ist an unsere im Impressum der Webseite angegebene Adresse zu richten.
			</div><br />
		</div>
	</div>
</body>

</html>