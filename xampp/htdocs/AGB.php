<html>
    
<head>
 <link rel="stylesheet" href="includes/styles/Stylesheet.css"> 
</head>
    
<body>

    <?php
		require("menu.php");
		?>
	<div id="AGB"> 
		<b> Allgemeine Gesch&auml;ftsbedingungen (AGB)</b>
		<p>
		<b> &sect; 1<br>
		Geltungsbereich &amp; Abwehrklausel</b><br>
		<p>
		1) F&uuml;r die &uuml;ber diesen Internet-Shop begr&uuml;ndeten Rechtsbeziehungen zwischen dem Betreiber des Shops (nachfolgend &quot;Anbieter&quot;) und seinen Kunden gelten ausschlie&szlig;lich die folgenden Allgemeinen Gesch&auml;ftsbedingungen in der jeweiligen Fassung zum Zeitpunkt der Bestellung.<br><br>
		(2) Abweichende Allgemeine Gesch&auml;ftsbedingungen des Kunden werden zur&uuml;ckgewiesen.
		</p><br>
		<b> &sect; 2<br>
		Zustandekommen des Vertrages</b><br>
		<p>
		(1) Die Pr&auml;sentation der Waren im Internet-Shop stellt kein bindendes Angebot des Anbieters auf Abschluss eines Kaufvertrages dar. Der Kunde wird hierdurch lediglich aufgefordert, durch eine Bestellung ein Angebot abzugeben.<br><br>

		(2) Durch das Absenden der Bestellung im Internet-Shop gibt der Kunde ein verbindliches Angebot gerichtet auf den Abschluss eines Kaufvertrages &uuml;ber die im Warenkorb enthaltenen Waren ab. Mit dem Absenden der Bestellung erkennt der Kunde auch diese Gesch&auml;ftsbedingungen als f&uuml;r das Rechtsverh&auml;ltnis mit dem Anbieter allein ma&szlig;geblich an.<br><br>

		(3) Der Anbieter best&auml;tigt den Eingang der Bestellung des Kunden durch Versendung einer Best&auml;tigungs-E-Mail. Diese Bestellbest&auml;tigung stellt noch nicht die Annahme des Vertragsangebotes durch den Anbieter dar. Sie dient lediglich der Information des Kunden, dass die Bestellung beim Anbieter eingegangen ist. Die Erkl&auml;rung der Annahme des Vertragsangebotes erfolgt durch die Auslieferung der Ware oder eine ausdr&uuml;ckliche Annahmeerkl&auml;rung.<br><br></p><br>
		<b> &sect; 3<br>
		Eigentumsvorbehalt</b><br>
		<p>
		Die gelieferte Ware verbleibt bis zur vollst&auml;ndigen Bezahlung im Eigentum des Anbieters.
		</p><br>
		<b> &sect; 4<br>
		F&auml;lligkeit</b><br>
		<p>
		Die Zahlung des Kaufpreises ist mit Vertragsschluss f&auml;llig.
		</p><br>
		<b> &sect; 5<br>
		Gew&auml;hrleistung</b><br>
		<p>
		(1) Die Gew&auml;hrleistungsrechte des Kunden richten sich nach den allgemeinen gesetzlichen Vorschriften, soweit nachfolgend nichts anderes bestimmt ist. F&uuml;r Schadensersatzanspr&uuml;che des Kunden gegen&uuml;ber dem Anbieter gilt die Regelung in &sect; 6 dieser AGB.<br><br>

		(2) Die Verj&auml;hrungsfrist f&uuml;r Gew&auml;hrleistungsanspr&uuml;che des Kunden betr&auml;gt bei Verbrauchern bei neu hergestellten Sachen 2 Jahre, bei gebrauchten Sachen 1 Jahr. Gegen&uuml;ber Unternehmern betr&auml;gt die Verj&auml;hrungsfrist bei neu hergestellten Sachen und bei gebrauchten Sachen 1 Jahr. Die vorstehende Verk&uuml;rzung der Verj&auml;hrungsfristen gilt nicht f&uuml;r Schadensersatzanspr&uuml;che des Kunden aufgrund einer Verletzung des Lebens, des K&ouml;rpers, der Gesundheit sowie f&uuml;r Schadensersatzanspr&uuml;che aufgrund einer Verletzung wesentlicher Vertragspflichten. Wesentliche Vertragspflichten sind solche, deren Erf&uuml;llung zur Erreichung des Ziels des Vertrags notwendig ist, z.B. hat der Anbieter dem Kunden die Sache frei von Sach- und Rechtsm&auml;ngeln zu &uuml;bergeben und das Eigentum an ihr zu verschaffen. Die vorstehende Verk&uuml;rzung der Verj&auml;hrungsfristen gilt ebenfalls nicht f&uuml;r Schadensersatzanspr&uuml;che, die auf einer vors&auml;tzlichen oder grob fahrl&auml;ssigen Pflichtverletzung des Anbieters, seiner gesetzlichen Vertreter oder Erf&uuml;llungsgehilfen beruhen. Gegen&uuml;ber Unternehmern ebenfalls ausgenommen von der Verk&uuml;rzung der Verj&auml;hrungsfristen ist der R&uuml;ckgriffsanspruch nach &sect; 478 BGB.<br><br>

		(3) Eine Garantie wird von dem Anbieter nicht erkl&auml;rt. 
		</p><br>
		<b> &sect; 6<br>
		Haftungsausschluss</b><br>
		<p>
		(1) Schadensersatzanspr&uuml;che des Kunden sind ausgeschlossen, soweit nachfolgend nichts anderes bestimmt ist. Der vorstehende Haftungsausschluss gilt auch zugunsten der gesetzlichen Vertreter und Erf&uuml;llungsgehilfen des Anbieters, sofern der Kunde Anspr&uuml;che gegen diese geltend macht.<br><br>

		(2) Von dem unter Ziffer 1 bestimmten Haftungsausschluss ausgenommen sind Schadensersatzanspr&uuml;che aufgrund einer Verletzung des Lebens, des K&ouml;rpers, der Gesundheit und Schadensersatzanspr&uuml;che aus der Verletzung wesentlicher Vertragspflichten. Wesentliche Vertragspflichten sind solche, deren Erf&uuml;llung zur Erreichung des Ziels des Vertrags notwendig ist, z.B. hat der Anbieter dem Kunden die Sache frei von Sach- und Rechtsm&auml;ngeln zu &uuml;bergeben und das Eigentum an ihr zu verschaffen. Von dem Haftungsausschluss ebenfalls ausgenommen ist die Haftung f&uuml;r Sch&auml;den, die auf einer vors&auml;tzlichen oder grob fahrl&auml;ssigen Pflichtverletzung des Anbieters, seiner gesetzlichen Vertreter oder Erf&uuml;llungsgehilfen beruhen.<br><br>

		(3) Vorschriften des Produkthaftungsgesetzes (ProdHaftG) bleiben unber&uuml;hrt. 
		</p><br>
		<b> &sect; 7<br>
		Abtretungs- und Verpf&auml;ndungsverbot</b><br>
		<p>
		Die Abtretung oder Verpf&auml;ndung von dem Kunden gegen&uuml;ber dem Anbieter zustehenden Anspr&uuml;chen oder Rechten ist ohne Zustimmung des Anbieters ausgeschlossen, sofern der Kunde nicht ein berechtigtes Interesse an der Abtretung oder Verpf&auml;ndung nachweist.
		</p><br>
		<b> &sect; 8<br>
		Aufrechnung</b><br>
		<p>
		Ein Aufrechnungsrecht des Kunden besteht nur, wenn seine zur Aufrechnung gestellte Forderung rechtskr&auml;ftig festgestellt wurde oder unbestritten ist. 
		</p><br>
		<b> &sect; 9<br>
		Rechtswahl &amp; Gerichtsstand</b><br>
		<p>
		(1) Auf die vertraglichen Beziehungen zwischen dem Anbieter und dem Kunden findet das Recht der Bundesrepublik Deutschland Anwendung. Von dieser Rechtswahl ausgenommen sind die zwingenden Verbraucherschutzvorschriften des Landes, in dem der Kunde seinen gew&ouml;hnlichen Aufenthalt hat. Die Anwendung des UN-Kaufrechts ist ausgeschlossen.<br><br>

		(2) Gerichtsstand f&uuml;r alle Streitigkeiten aus dem Vertragsverh&auml;ltnis zwischen dem Kunden und dem Anbieter ist der Sitz des Anbieters, sofern es sich bei dem Kunden um einen Kaufmann, eine juristische Person des &ouml;ffentlichen Rechts oder ein &ouml;ffentlich-rechtliches Sonderverm&ouml;gen handelt. 
		</p><br>
		<b> &sect; 10<br>
		Salvatorische Klausel</b><br>
		<p>
		Sollte eine Bestimmung dieser Allgemeinen Gesch&auml;ftsbedingungen unwirksam sein, wird davon die Wirksamkeit der &uuml;brigen Bestimmungen nicht ber&uuml;hrt.
		</p><br>
	</div>
    
</body>

</html>