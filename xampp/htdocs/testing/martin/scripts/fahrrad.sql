﻿-- phpMyAdmin SQL Dump

-- version 4.5.1

-- http://www.phpmyadmin.net

--
-- Host: 127.0.0.1

-- Erstellungszeit: 24. Mai 2016 um 23:39

-- Server-Version: 10.1.13-MariaDB

-- PHP-Version: 5.6.21

SET 
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET time_zone = "+00:00";



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;

/*!40101 SET NAMES utf8mb4 */;


--
-- Datenbank: `fantastic4_bikes`
--

-- 
--------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fahrrad`
--


CREATE TABLE `fahrrad` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `grundpreis` decimal(10,2) NOT NULL
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Daten für Tabelle `fahrrad`
--


INSERT INTO `fahrrad` (`id`, `name`, `grundpreis`) 
VALUES
(1, 'e-bike', '1500.00'),
(2, 'mountainbike', '1000.00'),

(3, 'Trekkingbike', '700.00'),
(4, 'Citybike', '400.00');


--
-- Indizes der exportierten Tabellen
--

--
-- 
Indizes für die Tabelle `fahrrad`
--

ALTER TABLE `fahrrad`
  
ADD PRIMARY KEY (`id`);


--
-- AUTO_INCREMENT für exportierte Tabellen
--

-
-
-- AUTO_INCREMENT für Tabelle `fahrrad`
--

ALTER TABLE `fahrrad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
