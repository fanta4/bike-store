<html>
    
<head>
 <link rel="stylesheet" href="includes/styles/Stylesheet.css"> 
</head>
    
<body>
	<?php
		require("menu.php");
	?>
<!-- Warenkorb -->
<div id="Warenkorb">
<table summary="Summe der Artikel im Warenkorb">
<caption>Produkte in Ihrem Warenkorb</caption>
<thead>
  <tr>
    <th scope="col">Anzahl</th>
    <th scope="col">Produkt</th>
    <th scope="col">Einzelpreis</th>
    <th scope="col">Gesamt</th>
  </tr>
</thead>
<tfoot>
  <tr>
    <td colspan="3">Summe Warenkorb:</td>
    <td>269,47 EUR</td>
  </tr>
</tfoot>
<tbody>
  <tr>
    <td><input type="text" size="3" value="1"></td>
    <td><a href="#">Digitalkamera: FujiFilm FinePix F100fd</a></td>
    <td>209,19 EUR</td>
    <td>209,19 EUR</td>
  </tr>
  <tr>
    <td><input type="text" size="3" value="2"></td>
    <td><a href="#">Speicherkarte: SanDisk Extreme III SD 4GB</a></td>
    <td>21,75 EUR</td>
    <td>43,50 EUR</td>
  </tr>
  <tr>
    <td><input type="text" size="3" value="1"></td>
    <td><a href="#">Akku: Fuji AccuPower</a></td>
    <td>6,29 EUR</td>
    <td>6,29 EUR</td>
  </tr>
  <tr>
    <td><input type="text" size="3" value="1"></td>
    <td><a href="#">Tasche: Fujifilm Softcase SC-F</a></td>
    <td>10,49 EUR</td>
    <td>10,49 EUR</td>
  </tr>
</tbody>
</table>
</div>
</body>

</html>