<html>
	<body>
		<?php
			session_start();
			require("menu.php");
			require("includes/connection.php");
			if(isset($_GET['chosen_sattle'])){
				if(isset($_SESSION['bike'])){
					// bike chosen and sattle chosen
					$_SESSION['sattle'] = $_GET['chosen_sattle'];
					$_SESSION['sattle_price'] = $_GET['sattle_price'];
					echo "<p>Please add a break for your " . $_SESSION['bike'] . " with " . $_SESSION['sattle'] . "</p>";
				}
			}else{
				//just joining the site
				unset($_SESSION['bike']);
				unset($_SESSION['bike_price']);
				unset($_SESSION['breaks']);
				unset($_SESSION['breaks_price']);
				echo "<p>Please select a break</p>";
			}				
			$sql= "SELECT * FROM zubehoer WHERE id > 0 AND art LIKE 'Bremsen'";
		
			if(!$result = $db->query($sql)){
				die(mysql_error());
			}
			//display breaks, if bike was chosen before add a collumn for fullprice
			echo "<table>";
			echo "<tr><td>" . "<b>Bremsen</b>" . "</td><td>" . "<b>Preis</b></td><td><b>Buy it</b></td>";
			if(isset($_GET['bike'])){
				echo "<td><b>Gesamt</b>" . "</td>";
			}
			echo "</tr>";
			while($row = $result->fetch_assoc()){
				$break = $row['name'];
				$price = $row['preis'];
				
				echo "<tr><td>" . $row['name'] . "</td><td>" . $row['preis'] . " EUR  </td>";
				if(isset($_SESSION['sattle']) && isset($_SESSION['bike']))
				{
					//bike and sattle are set --> add to cart
					echo "<td><a href='add_to_cart.php?breaks=$break&breaks_price=$price'>add to bike</a></td>";
					
				}else if(isset($_SESSION['bike'])){
					//not yet a sattle selected --> select a sattle
					echo "<td><a href='sattel.php?breaks=$break&breaks_price=$price'>add to bike</a></td>"; 
				}else{
					//just buying a sattle because no bike selected
					echo "<td><a href='add_to_cart.php?breaks=$break&breaks_price=$price'>add to cart</a></td>"; 
				}
				if(isset($_SESSION['bike'])){
					$fullprice = $price + $_SESSION['bike_price'];
					if(isset($_SESSION['sattle']))
					{
						$fullprice = $fullprice + $_SESSION['sattle_price'];
					}
					echo "<td>" . $fullprice ." EUR  </td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			$result->free();
			
		?>
	</body>
</html>