<html>
	<body>
		<?php
			session_start();
			require("menu.php");
			require("includes/connection.php");
			if(isset($_GET['chosen_bike'])){
				//set session variables if bike was chosen before
				$_SESSION['bike'] = $_GET['chosen_bike'];
				$_SESSION['bike_price'] =  $_GET['bike_price'];
				echo "<p>Please add a Sattel for your " . $_GET['chosen_bike'] . ". Afterwards you will be directed to select the breaks</p>";
				
			}else if(isset($_GET['breaks']) && isset($_SESSION['bike'])){
				
				//configuring a bike and breaks were chosen before
				echo "<p>Please add a Sattel for your " . $_SESSION['bike'] . ". with " . $_GET['breaks'] . ".</p>";
				$_SESSION['breaks'] = $_GET['breaks'];
				$_SESSION['breaks_price'] = $_GET['breaks_price'];
				
			}else{
				//just joining the site, unset session variables from before
				unset($_SESSION['bike']);
				unset($_SESSION['bike_price']);
				unset($_SESSION['breaks']);
				unset($_SESSION['breaks_price']);
				echo "<p>Please select a Sattel</p>";
			}
			$sql= "SELECT * FROM zubehoer WHERE id > 0 AND art LIKE 'Sattel'";
		
			if(!$result = $db->query($sql)){
				die(mysql_error());
			}
			//display sattels, if bike was chosen before add a collumn for fullprice
			echo "<table>";
			echo "<tr><td>" . "<b>Sattel</b>" . "</td><td>" . "<b>Preis</b></td><td><b>Buy it</b></td><td>";
			if(isset($_GET['chosen_bike'])){
				echo "<b>Gesamt</b>";
			}	
			echo "</td></tr>";
			while($row = $result->fetch_assoc()){
				$chosen = $row['name'];
				$price = $row['preis'];
				
				echo "<tr><td>" . $row['name'] . "</td><td>" . $row['preis'] . " EUR  </td>";
				if(!isset($_SESSION['breaks'])&& isset($_SESSION['bike'])){
					//breaks are missing
					echo "<td><a href='bremsen.php?chosen_sattle=$chosen&sattle_price=$price'>add to bike1</a></td>"; 
				}else if(isset($_SESSION['breaks']) && isset($_SESSION['bike']))
				{
					//break and bike chosen, add sattle and the add bike to card
					echo "<td><a href='add_to_cart.php?chosen_sattle=$chosen&sattle_price=$price'>add to bike</a></td>";
				}else{
					//just buying a sattle
					echo "<td><a href='add_to_cart.php?chosen_sattle=$chosen&sattle_price=$price'>add to card</a></td>";
				}
				if(isset($_SESSION['bike'])){
					if(isset($SESSION_['breaks']))
					{
						$fullprice = $price + $_SESSION['bike_price'] + $_SESSION['break_price'];
					}else{
						$fullprice = $price + $_SESSION['bike_price'];	
					}
					echo "<td>" . $fullprice ." EUR  </td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			$result->free();
		?>
	</body>
</html>